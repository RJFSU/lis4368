> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS 4368

## Richard Bland

### Assignments

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Git
    - Set up BitBucket repository

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Install SQL if not already installed
	- Create links for hello, HelloHome.html, sayhello, querybook.html, and sayhi

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create a3 ERD
	- Include 10 records in each table
	- Forward engineer

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Upload assignment screenshots
	- Take screenshot of failed validation
	- Take screenshot of successful validation

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Upload assignment screenshots
	- Edit java fies
	- Compile java files
	- Take screenshot of failed validation
	- Take screenshot of successful validation

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Upload assignment screenshots
	- Edit java fies
	- Compile java files

6. [P2 README.md](p2/README.md "My P2 README.md file")
	- Upload assignment screenshots
	- MVC Framework
	- Client-side server validation
	- Prepared Statements
	- CRUD Functionality
