> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Project #1 Requirements:
	-Screenshot of Homepages
	-Screenshot of failed validation
	-Screenshot of successful validation
	-Research on validation codes

#### Validtaion Codes:
	valid: 'fa fa-check',
-Shown when the field is valid

	invalid: 'fa fa-times',
-Shown when the field is invalid

	validating: 'fa fa-refresh'
-Shown when the field is being validated

#### Assignment Screenshots:

*Screenshots of ERD*:

![failed validation screenshot 1](img/failedvalidation1.png)
![failed validation screenshot 2](img/failedvalidation2.png)
![successfull validation screenshot 1](img/successfulvalidation1.png)
![successfull validation screenshot 2](img/successfulvalidation2.png)
