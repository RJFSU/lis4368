> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Assignment # Requirements: Assignment1

*Sub-Heading:*

1. Distrubuted Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of runing http://localhost:9999 (#2 above, Step #4(b) in tutorial);
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketinstallationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - this command creates an empty git repository
2. git status - displays pathst that have differences between the index file and the current HEAD commit
3. git add - updates the index using the current content found in the working tree
4. git commit - stores the current contents of the index in a new commit along with a log message from the user describing the changes
5. git push - updates remote refs using local refs, while sending objects necessary to complete the given refs
6. git pull - incorporates changes from a remote repository into the current branch
7. git branch - if --list is given, or if there are no non-option arguments, existing branches are listed; the current branch will be highlighted with an asterisk

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


*Screenshot of tomcat running*:

![Tomcat Installation Screenshot](img/tomcat.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/RJFSU/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/RJFSU/myteamquotes/ "My Team Quotes Tutorial")
