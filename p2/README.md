> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Project #2 Requirements:
	-MVC Framework
	-Client-side server validation
	-JSTL for XSS attacks
	-CRUD Functionality
	-Assignment Screenshots (7)

#### Assignment Screenshots:


Valid User Form Entry Screenshot:
![Valid User Form Entry Screenshot](img/1.png)
Passed Validation Screenshot:
![Passed Validation Screenshot](img/2.png)
Display Data Screenshot:
![Display Data Screenshot](img/3.png)
Modify Form Screenshot:
![Modify Form Screenshot](img/4.png)
Modified Data Sceenshot:
![Modified Data Sceenshot](img/5.png)
Delete Warning Screenshot:
![Delete Warning Screenshot](img/6.png)
Database Changes Screenshot:
![Database Changes Screenshot](img/7.png)
Database Changes Screenshot #2:
![Database Changes Screenshot #2](img/8.png)
