> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Assignment #5 Requirements:
	-Edit java files
	-Create data folder in crud folder
	-Create CustomerDB.java
	-Create ConnectionPool.java
	-Create DBUtil.java
	-Make sure it populates the database

#### Assignment Screenshots:

*Screenshots of Validations*:

![Valid user form entry screenshot](img/formentry.png)
![Passed validation screenshot](img/passedvalidation.png)
![Associated Database Entry screenshot](img/database.png)
