> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Assignment # Requirements: Assignment2


#### README.md file should include the following items:

[http://localhost:9999/hello](http://localhost:9999/hello)


[http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)


[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)


[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)


[http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)


#### Assignment Screenshots:

*Screenshots of querybook.html*:

![querybook.html screenshots](img/querybook1.png)
![querybook.html screenshots](img/querybook2.png)
