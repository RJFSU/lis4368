> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Assignment # Requirements: Assignment 3


#### README.md file should include the following items:

[a3.mwb file](a3.mwb)


[a3.sql file](a3.sql)



#### Assignment Screenshots:

*Screenshots of ERD*:

![a3 ERD screenshot](img/a3ERD.png)
