> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Richard Bland

### Assignment #4 Requirements:
	-Successfully validate information on the server side
	-Edit customerform.jsp, thanks.jsp
	-Edit and compile Customer.java and CustomerListServlet.java

#### Assignment Screenshots:

*Screenshots of Validations*:

![failed validation screenshot](img/failedvalidation.png)
![successful validation screenshot](img/successfulvalidation.png)

